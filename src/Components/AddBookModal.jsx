import { Button, Form, Input, Modal, Space, Switch, Upload } from "antd";
import { UploadOutlined } from '@ant-design/icons';
import TextArea from "antd/es/input/TextArea";
import toast from "react-hot-toast";

export default function AddTaskModal({ isModalOpen, handleCancel, addTask }) {
    const [form] = Form.useForm();

    const onFinish = (values) => {
        console.log("Success:", values);

        const book = {
            title: values.title,
            author: values.author,
            introduction: values.introduction,
            coverImage: localStorage.getItem("coverImage"), // Recupera la imagen del localStorage
            completed: values.completed,
        };

        addTask(book);

        toast.success(`Libro "${values.title}" agregado correctamente!`);

        form.resetFields();
    };

    const onFinishFailed = (errorInfo) => {
        console.log("Failed:", errorInfo);
    };

    const onReset = () => {
        form.resetFields();
    };

    const customRequest = ({ file, onSuccess }) => {
        setTimeout(() => {
            const imageUrl = "https://example.com/placeholder-image.jpg";
            // Convierte la imagen a Base64 y guárdala en localStorage
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                localStorage.setItem("coverImage", reader.result);
                onSuccess(imageUrl);
            };
        }, 1000);
    };

    return (
        <Modal
            title="Agregar libro"
            open={isModalOpen} // Cambia "visible" a "open"
            onCancel={handleCancel}
            footer={false}
        >
            <Form
                initialValues={{ completed: false }}
                layout="vertical"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                form={form}
                autoComplete="off"
            >
                <Form.Item
                    label="Título"
                    name="title"
                    rules={[
                        {
                            required: true,
                            message: "Por favor ingresa el título del libro",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Autor"
                    name="author"
                    rules={[
                        {
                            required: true,
                            message: "Por favor ingresa el autor del libro",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Introducción"
                    name="introduction"
                    rules={[
                        {
                            required: true,
                            message: "Por favor ingresa la introducción del libro",
                        },
                    ]}
                >
                    <TextArea />
                </Form.Item>
                <Form.Item
                    label="Portada"
                    name="coverImage"
                    valuePropName="fileList"
                    getValueFromEvent={(e) => e.fileList}
                >
                    <Upload
                        customRequest={customRequest}
                        maxCount={1}
                        listType="picture"
                        beforeUpload={() => false}
                    >
                        <Button icon={<UploadOutlined />}>Subir Imagen</Button>
                    </Upload>
                </Form.Item>
                <Form.Item
                    label="Completado"
                    name="completed"
                    valuePropName="checked"
                >
                    <Switch />
                </Form.Item>
                <Form.Item>
                    <Space>
                        <Button type="primary" htmlType="submit">
                            Guardar
                        </Button>
                        <Button htmlType="reset" onClick={onReset}>
                            Limpiar
                        </Button>
                    </Space>
                </Form.Item>
            </Form>
        </Modal>
    );
}
