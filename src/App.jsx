import { Routes, Route, BrowserRouter } from "react-router-dom";
import "./App.css";
import BookDetailsPage from "./pages/BookDetailsPage";
import MainPage from "./pages/MainPage";

function App() {
    return (
        <BrowserRouter>
        
                <Routes>
                <Route path="/book/:id" element={<BookDetailsPage/>} />
                    <Route path="/" element={<MainPage />} />
                </Routes>
        </BrowserRouter>
    );
}

export default App;
