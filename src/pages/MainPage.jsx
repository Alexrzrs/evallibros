// MainPage.js
import { useState, useEffect } from "react";
import AddBookButton from "../Components/AddBookButton";
import AddBookModal from "../Components/AddBookModal";
import ViewListBooks from "./ViewListBooks";
import Toast from "../Components/Toast";
import { v4 as uuidv4 } from "uuid";

function MainPage() {
    const [books, setBooks] = useState(() => {
        return JSON.parse(localStorage.getItem("books")) || [];
    });

    const [isModalOpen, setIsModalOpen] = useState(false);
    const [toast, setToast] = useState({
        show: false,
        message: "",
    });

    useEffect(() => {
        localStorage.setItem("books", JSON.stringify(books));
    }, [books]);

    const showToast = (message) => {
        setToast({ show: true, message });
        setTimeout(() => {
            setToast({ show: false, message: "" });
        }, 3000);
    };

    const showModal = () => {
        setIsModalOpen(true);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    const addBook = (book) => {
        const newBook = { ...book, id: uuidv4() }; // Agrega un ID único al libro
        setBooks([...books, newBook]);
        setIsModalOpen(false);
        showToast(`Libro "${book.title}" agregado correctamente!`);
      }

    const handleCompleteBook = (index) => {
        const updatedDataBooks = [...books];
        updatedDataBooks[index].completed = true;
        setBooks(updatedDataBooks);
        showToast(`Libro completado: "${updatedDataBooks[index].title}"`);
    };

    return (
        <>
            <AddBookButton onClick={showModal} />
            <AddBookModal
                isModalOpen={isModalOpen}
                handleCancel={handleCancel}
                addTask={addBook}
            />
            <ViewListBooks
                dataBooks={books}
                handleCompleteBook={handleCompleteBook}
            />
            <Toast message={toast.message} show={toast.show} />
        </>
    );
}

export default MainPage;
