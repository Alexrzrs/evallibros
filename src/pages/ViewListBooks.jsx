import { Divider, Space, Card, Row, Col, Button, Input } from "antd";
import { useEffect, useState } from "react";
import { AiOutlineSearch, AiOutlineCheckCircle } from "react-icons/ai";
import { Link } from "react-router-dom"; // Importa Link

export default function ViewListBooks(props) {
    const [dataBooks, setDataBooks] = useState([]);
    const [noData, setNoData] = useState(true);
    const [searchText, setSearchText] = useState("");
    const isNewBookAdded = props.dataBooks.length > dataBooks.length;

    useEffect(() => {
        function getBooks() {
            if (isNewBookAdded) {
                setDataBooks(props.dataBooks);
            }
        }

        function checkData() {
            if (props.dataBooks.length === 0) {
                setNoData(true);
            } else {
                setNoData(false);
            }
        }

        getBooks();
        checkData();
    }, [props.dataBooks, isNewBookAdded]);

    const filteredData = dataBooks.filter((book) =>
        book.title.toLowerCase().includes(searchText.toLowerCase())
    );

    return (
        <Row className="contentTask">
            <Input
                placeholder="Buscar por título"
                value={searchText}
                onChange={(e) => setSearchText(e.target.value)}
                prefix={<AiOutlineSearch />}
                style={{ width: "60%", height: 50 }}
            />

            <Col span={24}>
                <h1 className="title">Listado de mis libros:</h1>
            </Col>

            {noData ? (
                <Col span={24}>
                    <h2>Por el momento no tienes libros registrados</h2>
                    {/* Agrega aquí una imagen para mostrar cuando no hay libros */}
                </Col>
            ) : null}
            {filteredData.length === 0 ? (
                <Col span={24}>
                    <h2>Sin libros encontrados</h2>
                </Col>
            ) : null}
            {filteredData.map((book, index) => {
                return (
                    <Col
                        className="alingCard"
                        xs={{ span: 24 }}
                        lg={{ span: 10, offset: 1 }}
                        key={index}
                    >
                       <Card className="cardTask">
  <p className="titleTask">{book.title}</p>
  <p>Autor: {book.author}</p>
  <Divider />
  <img src={localStorage.getItem("coverImage")} alt={book.title} />
  <p>Estado: {book.completed ? "Completado" : "Pendiente"}</p>
  <Space>
    <Link to={`/book/${book.id}`}>
      <Button className="btnTask" type="primary">
        Ver Detalles
      </Button>
    </Link>
    {!book.completed && (
      <Button
        className="btnTask"
        type="primary"
        shape="circle"
        size="large"
        color="blue"
        onClick={() => props.handleCompleteBook(index)}
      >
        <AiOutlineCheckCircle size={40} />
      </Button>
    )}
  </Space>
</Card>

                    </Col>
                );
            })}
        </Row>
    );
}
