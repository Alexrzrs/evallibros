import React from "react";
import { Link, useParams } from "react-router-dom";
import { Card } from "antd";

function BookDetailsPage() {
  const { id } = useParams();

  // Recupera los datos de libros del local storage
  const dataBooks = JSON.parse(localStorage.getItem("books"));

  // Encuentra el libro con el ID correspondiente
  const book = dataBooks.find((book) => book.id === id);

  if (!book) {
    return (
      <div style={styles.container}>
        <h2>Libro no encontrado</h2>
        <Link to="/" style={styles.link}>
          Volver a la página de inicio
        </Link>
      </div>
    );
  }

  return (
    <div style={styles.container}>
      <Link to="/" style={styles.link}>
        Volver a la página de inicio
      </Link>
      <Card style={styles.card}>
        <h1>{book.title}</h1>
        <p>Autor: {book.author}</p>
        <p>Descripción: {book.description}</p>
        <img src={book.coverImage} alt={book.title} style={styles.image} />
        <p>Estado: {book.completed ? "Completado" : "Pendiente"}</p>
      </Card>
    </div>
  );
}

const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
  },
  link: {
    marginBottom: "10px",
    textDecoration: "none",
  },
  card: {
    width: "80%",
    maxWidth: "400px",
    padding: "20px",
    textAlign: "center",
  },
  image: {
    maxWidth: "100%",
  },
};

export default BookDetailsPage;
